﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private MyIni ini = new MyIni();

        private const string SECTION_NAME = "AIRS";
        private const string GROUP_KEY = "group";
        private const string LAST_INCIDENT_KEY = "last-incident";
        private const string DAY_DURATION = "day-duriation";
        private const int REINITIALIZE_TICKS = 1000;

        private DateTime lastDayUpdate = DateTime.MinValue;
        private bool firstInit = true;
        private int tickCounter;

        private int dayDuration;
        private Dictionary<string, Group> groups = new Dictionary<string, Group>();

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update100;
            tickCounter = REINITIALIZE_TICKS;
        }

        public void Main(string argument, UpdateType updateSource)
        {
            tickCounter++;

            bool reinitialize = false;
            if (updateSource == UpdateType.Terminal || updateSource == UpdateType.Trigger)
            {
                if (argument.Length == 0)
                {
                    reinitialize = true;
                }
                else
                {
                    if (groups.ContainsKey(argument))
                    {
                        var group = groups[argument];
                        group.incident();
                        Save();
                    }
                    else
                    {
                        Echo("group not found");
                    }
                }
            }

            if (tickCounter >= REINITIALIZE_TICKS || reinitialize)
            {
                Echo("reinitialize");
                initializeBlocks();
            }

            Echo("Incident Groups:");

            groups.Values.ToList().ForEach(delegate (Group group)
            {
                Echo(group.id + " " + group.daysSinceIncident() + " days");
            });

            if ((DateTime.UtcNow - lastDayUpdate).TotalSeconds > dayDuration)
            {
                groups.Values.ToList().ForEach(delegate (Group group) { group.dayUpdate(); });
            }
        }

        public void initializeBlocks()
        {
            ini.Clear();

            ini.TryParse(Me.CustomData);
            bool unconfigured = false;
            if (!ini.ContainsSection(SECTION_NAME))
            {
                unconfigured = true;
                ini.AddSection(SECTION_NAME);
            }

            var durationKey = new MyIniKey(SECTION_NAME, DAY_DURATION);
            if (!ini.ContainsKey(durationKey))
            {
                unconfigured = true;
                ini.Set(durationKey, 3600);
                ini.SetComment(durationKey, "Time of an ingame Day in seconds (3600 = 1h)");
            }

            if (unconfigured)
            {
                Me.CustomData = ini.ToString();
                Runtime.UpdateFrequency = UpdateFrequency.None;
                Echo("Default configuration created in CustomData. Check configuration and restart Program");
                return;
            }

            dayDuration = ini.Get(durationKey).ToInt32();

            if (firstInit)
            {
                loadGroupData();
                firstInit = false;
            }
            else
            {
                groups.Values.ToList().ForEach(delegate (Group group)
                {
                    group.displays = new List<IMyTextSurface>();
                });
            }

            var surfaceProvider = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType(surfaceProvider, b => b is IMyTextSurfaceProvider);

            surfaceProvider.ForEach(delegate (IMyTerminalBlock provider)
            {
                ini.Clear();
                ini.TryParse(provider.CustomData);
                var key = new MyIniKey(SECTION_NAME, GROUP_KEY);
                if (ini.ContainsKey(key))
                {
                    var groupId = ini.Get(key).ToString();
                    if (!groups.ContainsKey(groupId))
                    {
                        groups.Add(groupId, new Group(groupId, dayDuration));
                    }
                    var group = groups[groupId];
                    group.displays.Add((provider as IMyTextSurfaceProvider).GetSurface(0));
                }
            });

            var emptyGroups = new List<String>();
            groups.Values.ToList().ForEach(delegate (Group group)
            {
                if (group.displays.Count == 0)
                {
                    emptyGroups.Add(group.id);
                }
            });

            emptyGroups.ForEach(delegate (string id) { groups.Remove(id); });

            tickCounter = 0;
        }

        public void Save()
        {
            MyIni ini = new MyIni();
            groups.Values.ToList().ForEach(delegate (Group group)
            {
                ini.AddSection(group.id);
                ini.Set(group.id, LAST_INCIDENT_KEY, (group.lastIncident - DateTime.MinValue).TotalSeconds);
            });

            Storage = ini.ToString();
        }

        public void loadGroupData()
        {
            MyIni ini = new MyIni();
            ini.TryParse(Storage);

            var sections = new List<String>();
            ini.GetSections(sections);

            sections.ForEach(delegate (string id)
            {
                var lastIncident = DateTime.MinValue.AddSeconds(ini.Get(id, LAST_INCIDENT_KEY).ToInt32());
                var group = new Group(id, dayDuration, lastIncident);
                groups.Add(id, group);
            });
        }

        class Group
        {
            readonly public string id;
            readonly private int dayDuration;
            public List<IMyTextSurface> displays;
            public DateTime lastIncident;
            private DateTime lastUpdate;

            public Group(string id, int dayDuration) : this(id, dayDuration, DateTime.Now)
            { }

            public Group(string id, int dayDuration, DateTime lastIncident)
            {
                this.id = id;
                this.dayDuration = dayDuration;
                this.displays = new List<IMyTextSurface>();
                this.lastIncident = lastIncident;
                lastUpdate = DateTime.MinValue;
            }

            public void incident()
            {
                lastIncident = DateTime.UtcNow;
                updateDisplays();
            }

            public void dayUpdate()
            {
                if ((DateTime.UtcNow - lastUpdate).TotalSeconds >= dayDuration)
                {
                    updateDisplays();
                }
            }

            private void updateDisplays()
            {
                var finalDaysWithoutIncident = daysSinceIncident();
                displays.ForEach(delegate (IMyTextSurface textSurface)
                {
                    textSurface.WriteText("" + finalDaysWithoutIncident);
                });
                lastUpdate = DateTime.UtcNow;
            }

            public int daysSinceIncident()
            {
                return ((int)((DateTime.UtcNow - lastIncident).TotalSeconds) / dayDuration);
            }
        }
    }
}
